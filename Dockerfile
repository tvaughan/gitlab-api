FROM registry.gitlab.com/tvaughan/docker-clojure:1.10
MAINTAINER "Tom Vaughan <tvaughan@tocino.cl>"

COPY ./gitlab-api /opt/gitlab-api
WORKDIR /opt/gitlab-api
