(defproject gitlab-api "0.1.0-SNAPSHOT"
  :main ^:skip-aot gitlab-api.core
  :target-path "target/%s"
  :bikeshed {:docstrings false
             :max-line-length 110})
