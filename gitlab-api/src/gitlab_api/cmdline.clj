(ns gitlab-api.cmdline
  (:require [gitlab-api.pipelines :as pipelines]
            [gitlab-api.projects :as projects])
  (:gen-class))

#_(+ 1 1)

(defn- cleanup
  [user-id]
  (doseq [project-id (projects/get-project-ids user-id)]
    (pipelines/delete-pipelines project-id)))

(defn -main
  [& args]
  (if args
    (doseq [user-id args]
      (cleanup user-id))
    (throw (Exception. "Usage: clojure -A:gitlab-api -m gitlab-api.cmdline user-id-1 user-id-2 ..."))))
