(ns gitlab-api.projects
  (:require [gitlab-api.utils :as utils]))

(defn get-project-ids
  [user-id]
  (utils/get-ids (utils/build-url "/users/" user-id "/projects/") []))
