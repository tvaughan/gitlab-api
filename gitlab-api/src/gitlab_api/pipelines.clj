(ns gitlab-api.pipelines
  (:require [gitlab-api.utils :as utils]))

(defn get-pipeline-ids
  [project-id]
  (utils/get-ids (utils/build-url "/projects/" project-id "/pipelines/") []))

(defn delete-pipeline
  [project-id pipeline-id]
  (let [pipeline-url (utils/build-url "/projects/" project-id "/pipelines/" pipeline-id)]
    (utils/DELETE pipeline-url)))

(defn delete-pipelines
  [project-id]
  (doseq [pipeline-id (get-pipeline-ids project-id)]
    (delete-pipeline project-id pipeline-id)))
