(ns gitlab-api.utils
  (:require [clj-http.client :as http-client]))

(defonce ^:private credentials {"PRIVATE-TOKEN" (System/getenv "GITLAB_PASSWORD")})

(defn build-url
  [& args]
  (apply str "https://gitlab.com/api/v4" args))

(defn make-request [method url & [opts]]
  (http-client/request
   (merge {:method method :url url :headers credentials :accept :json :as :auto} opts)))

(def CREATE (partial make-request :post))
(def READ (partial make-request :get))
(def UPDATE (partial make-request :patch))
(def DELETE (partial make-request :delete))

(defn get-ids
  [url ids]
  (if-not url
    ids
    (let [response (READ url)]
      (recur (-> response :links :next :href) (concat (map :id (:body response)) ids)))))
